Description: <short summary of the patch>
 TODO: Put a short summary on the line above and replace this paragraph
 with a longer explanation of this change. Complete the meta-information
 with other relevant fields (see below for details). To make it easier, the
 information below has been extracted from the changelog. Adjust it or drop
 it.
 .
 patool (1.12-4) UNRELEASED; urgency=medium
 .
   [ Ondřej Nový ]
   * d/control: Remove ancient X-Python-Version field
   * d/copyright: Use https protocol in Format field
   * d/changelog: Remove trailing whitespaces
   * d/control: Set Vcs-* to salsa.debian.org
 .
   [ Abhijith PA ]
   * Enable autopkgtest
Author: Ondřej Nový <onovy@debian.org>

---
The information above should follow the Patch Tagging Guidelines, please
checkout http://dep.debian.net/deps/dep3/ to learn about the format. Here
are templates for supplementary fields that you might want to add:

Origin: <vendor|upstream|other>, <url of original patch>
Bug: <url in upstream bugtracker>
Bug-Debian: https://bugs.debian.org/<bugnumber>
Bug-Ubuntu: https://launchpad.net/bugs/<bugnumber>
Forwarded: <no|not-needed|url proving that it has been forwarded>
Reviewed-By: <name and email of someone who approved the patch>
Last-Update: 2019-03-24

--- patool-1.12.orig/patool.egg-info/PKG-INFO
+++ patool-1.12/patool.egg-info/PKG-INFO
@@ -1,10 +1,12 @@
-Metadata-Version: 1.1
+Metadata-Version: 1.2
 Name: patool
 Version: 1.12
 Summary: portable archive file manager
 Home-page: http://wummel.github.io/patool/
 Author: Bastian Kleineidam
 Author-email: bastian.kleineidam@web.de
+Maintainer: Bastian Kleineidam
+Maintainer-email: bastian.kleineidam@web.de
 License: GPL
 Description: Various archive formats can be created, extracted, tested, listed,
         searched, compared and repacked by patool. The advantage of patool
--- /dev/null
+++ patool-1.12/tests/data/t.shar
@@ -0,0 +1,144 @@
+#!/bin/sh
+# This is a shell archive (produced by GNU sharutils 4.11).
+# To extract the files from this archive, save it to some FILE, remove
+# everything before the `#!/bin/sh' line above, then type `sh FILE'.
+#
+lock_dir=_sh09682
+#
+# Existing files will *not* be overwritten, unless `-c' is specified.
+#
+# This shar contains:
+# length mode       name
+# ------ ---------- ------------------------------------------
+#      2 -rw-r--r-- t.txt
+#
+MD5SUM=${MD5SUM-md5sum}
+f=`${MD5SUM} --version | egrep '^md5sum .*(core|text)utils'`
+test -n "${f}" && md5check=true || md5check=false
+${md5check} || \
+  echo 'Note: not verifying md5sums.  Consider installing GNU coreutils.'
+if test "X$1" = "X-c"
+then keep_file=''
+else keep_file=true
+fi
+echo=echo
+save_IFS="${IFS}"
+IFS="${IFS}:"
+gettext_dir=
+locale_dir=
+set_echo=false
+
+for dir in $PATH
+do
+  if test -f $dir/gettext \
+     && ($dir/gettext --version >/dev/null 2>&1)
+  then
+    case `$dir/gettext --version 2>&1 | sed 1q` in
+      *GNU*) gettext_dir=$dir
+      set_echo=true
+      break ;;
+    esac
+  fi
+done
+
+if ${set_echo}
+then
+  set_echo=false
+  for dir in $PATH
+  do
+    if test -f $dir/shar \
+       && ($dir/shar --print-text-domain-dir >/dev/null 2>&1)
+    then
+      locale_dir=`$dir/shar --print-text-domain-dir`
+      set_echo=true
+      break
+    fi
+  done
+
+  if ${set_echo}
+  then
+    TEXTDOMAINDIR=$locale_dir
+    export TEXTDOMAINDIR
+    TEXTDOMAIN=sharutils
+    export TEXTDOMAIN
+    echo="$gettext_dir/gettext -s"
+  fi
+fi
+IFS="$save_IFS"
+if (echo "testing\c"; echo 1,2,3) | grep c >/dev/null
+then if (echo -n test; echo 1,2,3) | grep n >/dev/null
+     then shar_n= shar_c='
+'
+     else shar_n=-n shar_c= ; fi
+else shar_n= shar_c='\c' ; fi
+f=shar-touch.$$
+st1=200112312359.59
+st2=123123592001.59
+st2tr=123123592001.5 # old SysV 14-char limit
+st3=1231235901
+
+if touch -am -t ${st1} ${f} >/dev/null 2>&1 && \
+   test ! -f ${st1} && test -f ${f}; then
+  shar_touch='touch -am -t $1$2$3$4$5$6.$7 "$8"'
+
+elif touch -am ${st2} ${f} >/dev/null 2>&1 && \
+   test ! -f ${st2} && test ! -f ${st2tr} && test -f ${f}; then
+  shar_touch='touch -am $3$4$5$6$1$2.$7 "$8"'
+
+elif touch -am ${st3} ${f} >/dev/null 2>&1 && \
+   test ! -f ${st3} && test -f ${f}; then
+  shar_touch='touch -am $3$4$5$6$2 "$8"'
+
+else
+  shar_touch=:
+  echo
+  ${echo} 'WARNING: not restoring timestamps.  Consider getting and
+installing GNU `touch'\'', distributed in GNU coreutils...'
+  echo
+fi
+rm -f ${st1} ${st2} ${st2tr} ${st3} ${f}
+#
+if test ! -d ${lock_dir} ; then :
+else ${echo} "lock directory ${lock_dir} exists"
+     exit 1
+fi
+if mkdir ${lock_dir}
+then ${echo} "x - created lock directory ${lock_dir}."
+else ${echo} "x - failed to create lock directory ${lock_dir}."
+     exit 1
+fi
+# ============= t.txt ==============
+if test -n "${keep_file}" && test -f 't.txt'
+then
+${echo} "x - SKIPPING t.txt (file already exists)"
+else
+${echo} "x - extracting t.txt (binary)"
+  sed 's/^X//' << 'SHAR_EOF' | uudecode &&
+begin 600 t.txt
+"-#)B
+`
+end
+SHAR_EOF
+  (set 20 12 05 25 20 11 37 't.txt'
+   eval "${shar_touch}") && \
+  chmod 0644 't.txt'
+if test $? -ne 0
+then ${echo} "restore of t.txt failed"
+fi
+  if ${md5check}
+  then (
+       ${MD5SUM} -c >/dev/null 2>&1 || ${echo} 't.txt': 'MD5 check failed'
+       ) << \SHAR_EOF
+a1d0c6e83f027327d8461063f4ac58a6  t.txt
+SHAR_EOF
+  else
+test `LC_ALL=C wc -c < 't.txt'` -ne 2 && \
+  ${echo} "restoration warning:  size of 't.txt' is not 2"
+  fi
+fi
+if rm -fr ${lock_dir}
+then ${echo} "x - removed lock directory ${lock_dir}."
+else ${echo} "x - failed to remove lock directory ${lock_dir}."
+     exit 1
+fi
+exit 0
--- /dev/null
+++ patool-1.12/tests/data/t.txt
@@ -0,0 +1 @@
+42
\ No newline at end of file
--- /dev/null
+++ patool-1.12/tests/data/t.txt.a
@@ -0,0 +1,3 @@
+!<arch>
+t.txt           1338145476  501   20    100640  2         `
+42
\ No newline at end of file
--- /dev/null
+++ patool-1.12/tests/data/t.txt.a.foo
@@ -0,0 +1,3 @@
+!<arch>
+t.txt           1338145476  501   20    100640  2         `
+42
\ No newline at end of file
--- /dev/null
+++ patool-1.12/tests/data/t/t.txt
@@ -0,0 +1 @@
+42
\ No newline at end of file
--- /dev/null
+++ patool-1.12/tests/data/t2.txt
@@ -0,0 +1 @@
+42
\ No newline at end of file
